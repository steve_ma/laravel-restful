<?php
namespace Stevema\Restful;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;

class RestfulController extends BaseController
{
    /**
     * 过滤插件  列表传过来的参数需要一些方法来走到模型里面
     * 请继承 Stevema\Restful\Filters\RestfulFilter
     * 可以没有 但是不能乱搞
     */
    protected ?string $filterClass = \Stevema\Restful\RestfulFilter::class;
    /**
     * 模型 对应的表
     * 应当继承 Illuminate\Database\Eloquent\Model
     */
    protected ?string $modelClass = null;

    /**
     * 模型解释器 -> 模型查出来的数据 有些不想放出去的 就可以使用这个来处理
     * 应当继承 Illuminate\Http\Resources\Json\JsonResource
     */
    protected ?string $resourceClass = null;

    /**
     * request ->  post  put patch 使用到的表单验证 - 表单验证还包含权限验证
     * 应当继承 Illuminate\Foundation\Http\FormRequest
     * 继承其他的验证也可以
     */
    protected ?string $requestClass = null;
    /**
     * 权限验证
     */
    protected ?string $permissionClass = null;
    /**
     * 缓存 - -
     */
    protected ?string $cacheClass = null;

    /**
     * 路由绑定的参数名称
     * ROUTE_KEY 模型在路由中应当保持一致 然后就是应当是Model的小写
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * ROUTE_KEYMAP 关系模型时用到的
     * 比如 Route::get('/smorders/{smorder}/smskus/{smsku}', function(){})
     * SmOrder 和 SmSku 可以是一对一 也可以是一对多
     * 这样就需要 ROUTE_KEY来确认接收哪个参数了
     * ROUTE_KEYMAP 来对应相关的值 -> 在smsku 中 如果smorder关联的是 order_id
     * ROUTE_KEYMAP = ['smorder' => 'order_id]  这样来与smorder对应
     *
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * 只有一个模型的时候不需要 ROUTE_KEY 和 ROUTE_KEYMAP
     *
     * @var string
     */
    protected ?string $routeKey = null;
    protected ?array $routeKeymap = [];
    /**
     * 获取路由id
     */
    protected function getRouteKey(){
        $key = $this->routeKey;
        if(empty($key)) $key = $this->getModelRouteName();
        return $key;
    }
    protected function getCurrentId()
    {
        $key = $this->getRouteKey();
        $value = request()->route($key);
        if(empty($value)){
            $params = $this->getRouteValues();
            return current($params);
        }
        return $value;
    }
    protected function getRouteValues(){
        return request()->route()->parameters;
    }
    protected function queryRouteValues($query){
        $key = $this->getRouteKey();
        $params = $this->getRouteValues();
        if(count($params) > 1) {
            foreach ($params as $k => $v) {
                if ($k != $key && isset($this->routeKeymap[$k])) {
                    $query->where($this->routeKeymap[$k], '=', $v);
                }
            }
        }
        return $query;
    }
    /**
     * 获取模型
     */
    protected function getModel()
    {
        return $this->modelClass;
    }

    /**
     * 获取模型的名字后小写
     * @return string
     */
    protected function getModelRouteName()
    {
        $arr = explode('\\', $this->modelClass);
        return strtolower(end($arr));
    }

    /**
     * 获取解释器
     */
    protected function getResource()
    {
        return $this->resourceClass;
    }

    /**
     * 获取request 表单验证
     */
    protected function getRequest()
    {
        $request     = app('request');
        if($this->requestClass){
            $request = $this->requestClass::createFrom($request);
        }
        return $request;
    }

    /**
     * 表单验证
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws PermissionException
     */
    protected function formRequestValidation($scene=null): void
    {
        $this->hasPermission();
        $request = $this->getRequest();
        $app     = app();
        $request->setContainer($app);//->setRedirector($app->make(Redirector::class));
        if(method_exists($request, 'scene')) {
            $request->scene($scene);
        }
        $request->validate();
    }

    /**
     * 修改数据的时候用到的-获取要修改的参数
     * @return array
     */
    protected function getFilteredPayload(): array
    {
        $model = $this->getModel();
        $model     = new $model();
        assert($model instanceof Model);

        $fillable = $model->getFillable();
        $request = $this->getRequest();
        return $fillable === [] ? $request->all() : $request->only($fillable);
    }

    /**
     * 获取过滤器
     * @return string
     */
    protected function getFilter()
    {
        return $this->filterClass;
    }

    /**
     * 获取模型数据
     * @param $method
     * @return mixed
     * @throws PermissionException
     */
    protected function getCurrentModel($method=null, $is_trashed=0){
        $currentId    = $this->getCurrentId();
        $query = $this->getQuery();
        if($method && method_exists($this, $method)){
            $query = $this->{$method}($query);
        }
        # 遍历路由里面的参数 并且query一下
        $query = $this->queryRouteValues($query);
        $currentModel = $query->findorfail($currentId);
        $this->hasObjectPermission($currentModel);
        return $currentModel;
    }

    /**
     * 获取query
     * @param $is_trashed
     * @return mixed
     * @throws PermissionException
     */
    protected function getQuery($is_trashed=0){
        $this->hasPermission();
        $model = $this->getModel();
        # 有没有加载 SoftDeletes
        if(method_exists($model, 'getDeletedAtColumn')){
            if($is_trashed == 1) {
                $query = $model::onlyTrashed();
            } else{
                $query = $model::withoutTrashed();
            }
        } else {
            $query = $model::query();
        }
        return $query;
    }

    /**
     * 获取权限
     * @return null
     */
    protected function getPermission(){
        return $this->permissionClass;
    }
    /**
     * 获取缓存模型
     * @return null
     */
    protected function getCache(){
        return $this->cacheClass;
    }

    /**
     * 判断有没有权限
     * @return void
     * @throws PermissionException
     */
    protected function hasPermission(): void
    {
        $permission = $this->getPermission();
        if(!is_null($permission)){
            if(!$permission::hasPermission()){
                throw new PermissionException("permission error", 403);
            }
        }
    }

    /**
     * 判断有没有 $currentModel 的权限
     * @param $currentModel
     * @return void
     * @throws PermissionException
     */
    protected function hasObjectPermission($currentModel): void
    {
        $permission = $this->getPermission();
        if(!is_null($permission)){
            if(!$permission::hasObjectPermission($currentModel)){
               throw new PermissionException("permission error", 403);
            }
        }
    }
}
