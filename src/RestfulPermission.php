<?php

namespace Stevema\Restful;

class RestfulPermission
{
    /**
     * 写权限的actions
     * @var string[]
     */
    public static $w_actions = ["store", "update", "destroy"];

    /**
     * 读权限的actions
     * @var string[]
     */
    public static $r_actions = ["index", "show"];

    /**
     * 列表的权限 index 会用到
     * @return bool
     */
    public static function hasPermission(): bool
    {
        return True;
    }

    /**
     * 获取currentModel后判断一下 有没有权限
     * @param $currentModel
     * @return bool
     */
    public static function hasObjectPermission($currentModel): bool
    {
        return True;
    }

    /**
     * 获取当前action的rwo情况
     * @return string r read w write o other
     */
    public static function getActionIsRW(){
        $action = request()->route()->getActionMethod();
        // index show  |  store update destroy
        if(in_array($action, self::$w_actions)) return 'w';
        if(in_array($action, self::$r_actions)) return 'r';
        return 'o';
    }
}
