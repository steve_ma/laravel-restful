<?php

namespace Stevema\Restful;

class RestfulFilterResource {
    public static function noPaginate($array, $cursor_key){
        return $array;
    }
    public static function paginate($array, $cursor_key){
        return [
            'results' => $array['data'],
            'count' => $array['total'],
            'next' => ($array['current_page'] < $array['last_page'])
                ? request()->fullUrlWithQuery([$cursor_key =>$array['current_page']+1])
                : null,
            'previous' => ($array['current_page'] > 1)
                ? request()->fullUrlWithQuery([$cursor_key =>$array['current_page']-1])
                : null,
        ];
    }
    public static function simplePaginate($array, $cursor_key){
        return [
            'results' => $array['data'],
            'next' => request()->fullUrlWithQuery([$cursor_key =>$array['current_page']+1]),
            'previous' =>($array['current_page'] > 1)
                ? request()->fullUrlWithQuery([$cursor_key =>$array['current_page']-1])
                : null,
        ];
    }
    public static function cursorPaginate($array, $cursor_key){
        return [
            'results' => $array['data'],
            'next' => empty($array['next_cursor'])
                ?null
                :request()->fullUrlWithQuery([$cursor_key =>$array['next_cursor']]),
            'previous' => empty($array['prev_cursor'])
                ? null
                : request()->fullUrlWithQuery([$cursor_key =>$array['prev_cursor']]),
        ];
    }
}
