<?php

namespace Stevema\Restful;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
//use Stevema\Restful\Traits\SoftDeletes;
use Stevema\Restful\Traits\UseTableNameAsMorphClass;

class RestfulModel extends Model
{
    use HasFactory;
//    use SoftDeletes;

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @return string
     */
//    protected function serializeDate(DateTimeInterface $date)
//    {
//        return $date->format('Y-m-d H:i:s');
//    }

    protected function hasSoftDeletes(){
        return method_exists($this, 'getSoftDeleteSetting');
    }

    /**
     * 设置软删除
     */
//    protected function getSoftDeleteSetting()
//    {
//        return [
//            "columnName" => 'is_del',
//            "columnType" => 'int',
//            "defaultValue" => 0,
//            "deletedValue" => 1,
//        ];
//    }
}
