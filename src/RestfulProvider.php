<?php

namespace Stevema\Restful;

use Illuminate\Support\ServiceProvider;

class RestfulProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->registerCommands();
    }

    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Stevema\Restful\Consoles\Commands\MakeControllerCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeFilterCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeModelCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeResourceCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeRequestCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeRestfulCommand::class,
                \Stevema\Restful\Consoles\Commands\MakePermissionCommand::class,
                \Stevema\Restful\Consoles\Commands\MakeCacheCommand::class,
            ]);
        }
    }
}
