<?php

namespace Stevema\Restful;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
class RestfulRequest extends FormRequest{
    public $scenes = [];
    public $currentScene;               //当前场景
    public $autoValidate = false;       //是否注入之后自动验证
    public function authorize()
    {
        return true;
    }
    /**
     * 设置场景
     * @param $scene
     * @return $this
     */
    public function scene($scene)
    {
        $this->currentScene = $scene;
        return $this;
    }
    /**
     * 覆盖自动验证方法
     */
    public function validateResolved()
    {
        if ($this->autoValidate) {
            $this->handleValidate();
        }
    }
    /**
     * 验证方法
     * @param string $scene
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate($scene = '')
    {
        if ($scene) {
            $this->currentScene = $scene;
        }
        $this->handleValidate();
    }
    /**
     * 根据场景获取规则
     * @return array|mixed
     */
    public function getRules()
    {
        $rules = $this->container->call([$this, 'rules']);
        $newRules = [];

        $method = request()->getMethod();
        if ($this->currentScene && isset($this->scenes[$this->currentScene])) {
            $sceneFields = is_array($this->scenes[$this->currentScene])
                ? $this->scenes[$this->currentScene] : explode(',', $this->scenes[$this->currentScene]);
            foreach ($sceneFields as $field) {
                if (array_key_exists($field, $rules)) {
                    if($method == 'PATCH'){
                        # patch 要把所有的required 换成 sometimes
                        $newRules[$field] = array_map(function($value){
                            if($value == 'required') return 'sometimes';
                            return $value;
                        }, $rules[$field]);
                    } else {
                        $newRules[$field] = $rules[$field];
                    }
                }
            }
            return $newRules;
        }
        return $rules;
    }
    /**
     * 覆盖设置 自定义验证器
     * @param $factory
     * @return mixed
     */
    public function validator($factory)
    {
        return $factory->make(
            $this->validationData(), $this->getRules(),
            $this->messages(), $this->attributes()
        );
    }
    /**
     * 最终验证方法
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function handleValidate()
    {
        $this->prepareForValidation();

        if (! $this->passesAuthorization()) {
            $this->failedAuthorization();
        }
        $instance = $this->getValidatorInstance();
        if ($instance->fails()) {
            $this->failedValidation($instance);
        }
        $this->passedValidation();
    }
    /**
     * 重写报错部分-适应API JSON下发的需求
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json($validator->errors()->messages(), 400)));
    }



    /**
     * 准备验证数据。验证前数据处理
     * 比如把user_id 从header中取出来放进去
     */
    protected function prepareForValidation(): void
    {
//        $user_id = Auth()->user()->id;
//        $data = [
//            'user_id' => $user->id,
//        ];
//        var_dump("准备验证数据。验证前数据处理");
//        // 覆盖其中的值
//        $this->merge(['name' => 'Taylor']);
//        // 替换所有的参数
//        $this->replace(['name' => 'Taylor']);
    }

    /**
     * 配置验证实例。- 验证后数据处理前 可以添加错误信息-
     * - - - 上面的规则总有一些是 不满足使用的
     * somethingElseIsInvalid 并不存在这个方法 就是告诉你这里可以有一些错误
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
//            if ($this->somethingElseIsInvalid()) {
//                $validator->errors()->add('field', 'Something is wrong with this field!');
//            }
        });
    }
    /**
     * 验证后数据处理
     * 有些数据是不想写入数据库的 比如 _token 等 就需要去掉
     * @return void
     */
    protected function passedValidation(): void
    {
        // 很多参数如果不在scenes里定义 则不应该存在 - 哪怕没有规则
//        var_dump("验证后数据处理");

        $method = request()->getMethod();

        if($method != 'PATCH'){
            $arr = $this->scenes[$this->currentScene];
            $data = [];
            if(!empty($arr)){
                foreach($arr as $key){
                    if(in_array($key, ["content"])) {
                        $data[$key] = $this->get($key);
                    } else {
                        $data[$key] = $this->$key;
                    }
                }
                $this->replace($data);
            }
        }

//        // 覆盖其中的值
//        $this->merge(['name' => 'Taylor']);
//        // 替换所有的参数
//        $this->replace(['name' => 'Taylor']);
    }



}
