<?php

namespace Stevema\Restful\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MakeRequestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:restrequest {name : 过滤器名称}
                            {--i|illustrate : 生成带说明的文件}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create request file with restful ';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");
        $illustrate = $this->option('illustrate');
        $className = $name;
        $namespace = '';

        $nameArr = explode('/', $name);
        if(count($nameArr) > 1){
            $className = end($nameArr);
            $namespace = "\\".str_replace(['/'.$className, '/'], ['','\\'], $name);
        }
        $will_path = app_path("Http/Requests/{$name}.php");
        if($illustrate) {
            $stub_path = realpath(__DIR__ . '/../Stubs/restful.request.illustrate.stub');
        } else {
            $stub_path = realpath(__DIR__ . '/../Stubs/restful.request.stub');
        }
        if(!File::exists($will_path)) {
            if(!File::isDirectory(File::dirname($will_path))){
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    ['{classname}', '{namespace}',],
                    [$className, $namespace, ],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'request', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'request', $will_path));
        }

        return 0;

    }
}
