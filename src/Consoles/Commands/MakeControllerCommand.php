<?php

namespace Stevema\Restful\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MakeControllerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:restcontroller {name : 控制器名称}
                            {--i|illustrate : 生成带说明的文件}
                            {--f|filter= : 过滤器::class}
                            {--m|model= : 模型::class}
                            {--r|resource= : 解释器::class}
                            {--c|cache= : 缓存::class}
                            {--R|request= : 表单验证::class}
                            {--p|permission= : 权限::class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create controler file with restful ';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");
        $illustrate = $this->option('illustrate');

        $filter = $this->option('filter');
        $model = $this->option('model');
        $resource = $this->option('resource');
        $request = $this->option('request');
        $permission = $this->option('permission');
        $cache = $this->option('cache');

        $model = empty($model) ? "null" : "\\".$model;
        $resource = empty($resource) ? "null" : "\\".$resource;
        $request = empty($request) ? "null" : "\\".$request;

        $iffilter = empty($filter) ? "" : "protected ?string \$filterClass = "."\\".$filter.";";
        $filter = empty($filter) ? "null" : "\\".$filter;
        $ifpermission = empty($permission) ? "" : "protected ?string \$permissionClass = "."\\".$permission.";";
        $permission = empty($permission) ? "null" : "\\".$permission;
        $ifcache = empty($cache) ? "" : "protected ?string \$cacheClass = "."\\".$cache.";";
        $cache = empty($cache) ? "null" : "\\".$cache;

        $className = $name;
        $namespace = '';

        $nameArr = explode('/', $name);
        if(count($nameArr) > 1){
            $className = end($nameArr);
            $namespace = "\\".str_replace(['/'.$className, '/'], ['','\\'], $name);
        }
        $will_path = app_path("Http/Controllers/{$name}.php");
        if($illustrate) {
            $stub_path = realpath(__DIR__ . '/../Stubs/restful.controller.illustrate.stub');
        } else {
            $stub_path = realpath(__DIR__ . '/../Stubs/restful.controller.stub');
        }
        if(!File::exists($will_path)) {
            if(!File::isDirectory(File::dirname($will_path))){
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    [
                        '{classname}', '{namespace}',
                        '{iffilter}', '{filter}',
                        '{ifpermission}', '{permission}',
                        '{ifcache}', '{cache}',
                        '{model}', '{resource}',
                        '{request}'
                    ],
                    [
                        $className, $namespace,
                        $iffilter, $filter,
                        $ifpermission, $permission,
                        $ifcache, $cache,
                        $model, $resource,
                        $request
                    ],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'controler', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'controler', $will_path));
        }

        return 0;

    }
}
