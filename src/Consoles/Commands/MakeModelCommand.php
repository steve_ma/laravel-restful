<?php

namespace Stevema\Restful\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MakeModelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:restmodel {name : 模型名称}
                            {--i|illustrate : 生成带说明的文件但是没什么用}
                            {--t|table= : 表名}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create model file with restful ';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");
        $illustrate = $this->option('illustrate');
        $table = $this->option('table');
        $className = $name;
        $namespace = '';

        $table = empty($table) ? "" : 'protected $table="'.$table.'";';

        $nameArr = explode('/', $name);
        if(count($nameArr) > 1){
            $className = end($nameArr);
            $namespace = "\\".str_replace(['/'.$className, '/'], ['','\\'], $name);
        }
        $will_path = app_path("Models/{$name}.php");
        $stub_path = realpath(__DIR__ . '/../Stubs/restful.model.stub');

        if(!File::exists($will_path)) {
            if(!File::isDirectory(File::dirname($will_path))){
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    ['{classname}', '{namespace}', '{table}',],
                    [$className, $namespace, $table,],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'model', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'model', $will_path));
        }

        return 0;

    }
}
