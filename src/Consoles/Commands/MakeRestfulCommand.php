<?php

namespace Stevema\Restful\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MakeRestfulCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:restful {name : 数据库表名称 比如 sm_test_t1}
                            {--i|illustrate : 生成带说明的文件}
                            {--t|test : 调试信息}
                            {--m|migration : 生成数据迁移文件}
                            {--s|seed : 生成数据填充文件}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create all restful file with restful ';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");
        $illustrate = $this->option('illustrate');
        $migration = $this->option('migration');
        $test = $this->option('test');
        $seed = $this->option('seed');
        $table_name = $name;
        $namespace = '';

        $nameArr = explode('/', $name);
        $nameTotal = count($nameArr);
        if($nameTotal > 1){
            $table_name = end($nameArr);
            $namespace = "\\".str_replace(['/'.$table_name, '/'], ['','\\'], $name);
        }
        # className 要驼峰
        $className = ucwords(str_replace("_", " ", $table_name));
        $className = str_replace(" ", "", $className);

        if($nameTotal > 1){
            $name_full = "";
            foreach($nameArr as $k =>$v){
                $name_full .= (empty($name_full) ? "" : "/") . (($k + 1) == $nameTotal ? $className : $v);
            }
        } else {
            $name_full = str_replace($table_name, $className, $name);
        }

        if($test){
            dd([
                "name" => $name,
                "className" => $className,
                "name_full" => $name_full,
                "table_name" => $table_name,
            ]);
        }

//        dd($illustrate);
        if($migration) {
            # 创建表
            $this->call('make:migration', [
                'name' => 'create_' . $table_name . 's_table',
                '--create' => $table_name.'s'
            ]);
        }
        if($seed) {
            # 填充文件
            $this->call('make:seed', [
                'name' => $className . "Seeder",
            ]);
        }
        # model
        $this->call('make:restmodel', [
            'name' => $name_full,
            '-t' => $table_name."s",
            '-i' => $illustrate,
        ]);
        # resource
        $this->call('make:restresource', [
            'name' => $name_full."Resource",
            '-i' => $illustrate,
        ]);
        # request
        $this->call('make:restrequest', [
            'name' => $name_full."Request",
            '-i' => $illustrate,
        ]);
        # request
        $this->call('make:restcache', [
            'name' => $name_full."Cache",
            '-i' => $illustrate,
        ]);
        # filter
        $this->call('make:restfilter', [
            'name' => $name_full."Filter",
            '-i' => $illustrate,
        ]);
        # permission
        $this->call('make:restpermission', [
            'name' => $name_full."Permission",
            '-i' => $illustrate,
        ]);
        # controller
        $this->call('make:restcontroller', [
            'name' => $name_full."Controller",
            '-i' => $illustrate,
            '-f' => "App\\Http\\Filters{$namespace}\\{$className}Filter::class",
            '-m' => "App\\Models{$namespace}\\{$className}::class",
            '-r' => "App\\Http\\Resources{$namespace}\\{$className}Resource::class",
            '-R' => "App\\Http\\Requests{$namespace}\\{$className}Request::class",
            '-p' => "App\\Http\\Permissions{$namespace}\\{$className}Permission::class",
            '-c' => "App\\Http\\Caches{$namespace}\\{$className}Cache::class",
        ]);


        return 0;

    }
}
