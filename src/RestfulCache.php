<?php

namespace Stevema\Restful;

class RestfulCache {
    /**
     * cache 实例
     * @var mixed|null
     */
    protected mixed $cache = null;

    /**
     * 设置cache实例
     * @return void
     */
    public function initCache(): void
    {
        $this->cache = cache();
    }

    /**
     * 过期时间 - 秒
     * @return int
     */
    public function expire(): int
    {
        return 86400;
    }

    /**
     * 当前类的类型 - 比如 user post badge 等
     * @return string|null
     */
    public function classType(): ?string
    {
        return '';
    }

    /**
     * 单个项目的key
     * @return string|null
     */
    public function itemKey(): ?string
    {
        return '__CLASSTYPE__:item:__ID__';
    }

    /**
     * 列表的key
     * @return string|null
     */
    public function listKey(): ?string
    {
        return '__CLASSTYPE__:list:__PARAMS__';
    }

    /**
     * 获取key用到的正则
     * @return string|null
     */
    public function getRegex(): ?string
    {
        return '/__(.*?)__/i';
    }

    /**
     * 实例化
     */
    public function __construct()
    {
        $this->initCache();
    }

    /**
     * 获取缓存的key
     * @param array $params
     * @param string $type
     * @return string|null
     */
    public function getCacheKey(array $params=[], string $type='item'): ?string
    {
        $key = $this->itemKey();

        if($type == 'list'){
            $key = $this->listKey();
        }
        preg_match_all($this->getRegex(), $key, $matches);
        if(empty($matches[1]) === FALSE) {
            foreach($matches[1] as $k => $v){
                $obj_key = strtolower($v);
                if($obj_key == 'params'){
                    $key = str_replace("__{$v}__", md5(json_encode($params)), $key);
                } else {
                    if(isset($params[$obj_key]) && $params[$obj_key]){
                        $key = str_replace("__{$v}__", $params[$obj_key], $key);
                    }
                }
            }
        }
        return $key;
    }

    /**
     * 获取单条
     * @param string|int $id
     * @param mixed|null $default
     * @param string $pk
     * @return mixed
     */
    public function getOne(string|int $id, mixed $default=null, string $pk='id'): mixed
    {
        $key = $this->getCacheKey([$pk => $id, 'classtype' => $this->classType()]);
        return $this->get($key, $default);
    }

    /**
     * 获取列表
     * @param array $params
     * @return mixed
     */
    public function getList(array $params=[]): mixed
    {
        $params['classtype'] = $this->classType();
        $key = $this->getCacheKey($params, 'list');
        return $this->get($key, null, 'list');
    }

    /**
     * 设置单条缓存
     * @param string|int $id
     * @param mixed $data
     * @param string $pk
     * @return mixed
     */
    public function setOne(string|int $id, mixed $data, string $pk='id'): mixed
    {
        $key = $this->getCacheKey([$pk => $id, 'classtype' => $this->classType()]);
        return $this->put($key, $data);
    }
    /**
     * 设置单条缓存
     * @param string|int $id
     * @param array $data
     * @param string $pk
     * @return mixed
     */
    public function forgetOne(string|int $id, string $pk='id'): mixed
    {
        $key = $this->getCacheKey([$pk => $id, 'classtype' => $this->classType()]);
        return $this->forget($key);
    }

    /**
     * 设置列表缓存
     * @param array $params
     * @param array $data
     * @return mixed
     */
    public function setList(array $params=[], array $data=[]): mixed
    {
        $params['classtype'] = $this->classType();
        $key = $this->getCacheKey($params, 'list');
        return $this->put($key, $data, 'list');
    }

    /**
     * 基础方法-获取缓存
     * @param string $key
     * @param mixed|null $default
     * @param string $type
     * @return mixed
     */
    public function get(string $key, mixed $default=null, string $type='item'): mixed
    {
        $classType = $this->classType();
        return $this->cache->tags([$classType, $type])->get($key, $default);
    }

    /**
     * 基础方法-设置缓存
     * @param string $key
     * @param mixed $values
     * @param string $type
     * @return mixed
     */
    public function put(string $key, mixed $values, string $type='item'): mixed
    {
        $expire = $this->expire();
        $classType = $this->classType();
        if($expire > 0){
            return $this->cache->tags([$classType, $type])->put($key, $values, $expire);
        } else {
            return $this->cache->tags([$classType, $type])->put($key, $values);
        }
    }
    /**
     * 基础方法-清除当前类的所有缓存
     * @param string $key
     * @param mixed $values
     * @return mixed
     */
    public function flush(string $type='item'): mixed
    {
        $classType = $this->classType();
        return $this->cache->tags([$classType, $type])->flush();
    }

    /**
     * 基础方法-忘记某个缓存
     * @param string $key
     * @param string $type
     * @return mixed
     */
    public function forget(string $key, string $type='item'): mixed
    {
        $classType = $this->classType();
        return $this->cache->tags([$classType, $type])->forget($key);
    }
}
