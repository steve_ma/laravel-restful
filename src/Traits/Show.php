<?php
namespace Stevema\Restful\Traits;

trait Show
{
    public function show(){
        $cache = $this->getCache();
        // 缓存
        if(!empty($cache)){
            $cacheClass = new $cache();
            $result = $cacheClass->getOne($this->getCurrentId());
            if(! empty($result)){
                return response()->json($result);
            }
        }
        # 获取模型数据
        $currentModel = $this->getCurrentModel('perShowQuery');
        # 获取资源解释器
        $resource = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }

        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->setOne($this->getCurrentId(), $currentModel);
        }
        return response()->json($currentModel);
    }
    public function perShowQuery($query){
        # 如果有想提前执行的过滤 这里可以使用
        # 比如 前端的接口 看article的时候必须发布的才能看得到
        # 还不能是预先设置发布时间的
//        $query->where("is_published", '=', 1);
//        $query->where("published_at", '<=', time());
        return $query;
    }
}
