<?php
namespace Stevema\Restful\Traits;

/**
 * 引入了SoftDeletes之后才能使用
 */
trait ForceDelete
{
    public function forceDelete(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel();
        $this->performForceDelete($currentModel);
        # 缓存
        $cache = $this->getCache();
        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->forgetOne($this->getCurrentId());
            $cacheClass->flush('list');
        }
        return response()->noContent();
    }
    public function performForceDelete($currentModel){
        $result = $currentModel->forceDelete();
        return $result;
    }
}
