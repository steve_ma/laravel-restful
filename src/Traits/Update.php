<?php
namespace Stevema\Restful\Traits;

use Stevema\Restful\RestfulException;

trait Update
{
    public function update(){
        # 注册验证表单->开始认证
        $this->formRequestValidation('update');
        # 获取验证后的参数
        $payload = $this->getFilteredPayload();
        # 获取模型数据
        $currentModel = $this->getCurrentModel();
        # 修改数据
        $currentModel = $this->performUpdate($currentModel, $payload);
        # 获取资源解释器
        $resource  = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        # 缓存
        $cache = $this->getCache();
        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->forgetOne($this->getCurrentId());
            $cacheClass->flush('list');
        }
        return response()->json($currentModel);
    }

    public function performUpdate($currentModel, $payload){
        $currentModel->update($payload);
        return $currentModel;
    }
}
