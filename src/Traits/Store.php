<?php
namespace Stevema\Restful\Traits;


use Stevema\Restful\RestfulException;

trait Store
{
    public function store(){
        # 注册验证表单->开始认证
        $this->formRequestValidation('store');
        # 获取验证后的参数
        $payload = $this->getFilteredPayload();
        # 插入数据
        $currentModel = $this->performStore($payload);
        # 获取资源解释器
        $resource  = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        # 缓存
        $cache = $this->getCache();
        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->flush('list');
        }
        return response()->json($currentModel, 201);
    }
    public function performStore($payload){
        $model = $this->getModel();
        $currentModel = $model::create($payload)->refresh();
        return $currentModel;
    }
}
