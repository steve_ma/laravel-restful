<?php
namespace Stevema\Restful\Traits;

/**
 * 引入了SoftDeletes之后才能使用
 */
trait Restore
{
    public function restore(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel(null, 1);
        # 恢复数据
        $this->performRestore($currentModel);
        # 获取资源解释器
        $resource = $this->getResource();
        if(!empty($resource)){
            $currentModel = new $resource($currentModel);
        }
        // 缓存
        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->forgetOne($this->getCurrentId());
            $cacheClass->flush('list');
        }

        return response()->json($currentModel);
    }

    public function performRestore($currentModel){
        $result = $currentModel->restore();
        return $result;
    }
}
