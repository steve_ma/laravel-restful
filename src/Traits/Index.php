<?php
namespace Stevema\Restful\Traits;

use Stevema\Restful\RestfulFilter;

trait Index
{
    public function index(){
        $cache = $this->getCache();
        // 缓存
        if(!empty($cache)){
            $cacheClass = new $cache();
            $result = $cacheClass->getList(request()->query());
            if(! empty($result)){
                return response()->json($result);
            }
        }
        $filters   = $this->getFilter();
        $query = $this->getQuery();
        $query = $this->perIndexQuery($query);
        if ($filters) {
            $filter = new $filters($query);
        } else {
            $filter = new RestfulFilter($query);
        }
        $resource = $this->getResource();
        $result = $filter->getData($resource);

        // 设置缓存
        if(!empty($cache)){
            $cacheClass->setList(request()->query(), $result);
        }
        return response()->json($result);
    }

    public function perIndexQuery($query){
        # 如果有想提前执行的过滤 这里可以使用
        # 比如 前端的接口 看article的时候必须发布的才能看得到
        # 还不能是预先设置发布时间的
//        $query->where("is_published", '=', 1);
//        $query->where("published_at", '<=', time());
        return $query;
    }
}
