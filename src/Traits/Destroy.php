<?php
namespace Stevema\Restful\Traits;

trait Destroy
{
    public function destroy(){
        # 获取模型数据
        $currentModel = $this->getCurrentModel();
        $this->performDestroy($currentModel);
        # 缓存
        $cache = $this->getCache();
        if(!empty($cache)){
            $cacheClass = new $cache();
            $cacheClass->forgetOne($this->getCurrentId());
            $cacheClass->flush('list');
        }
        return response()->noContent();
    }
    public function performDestroy($currentModel){
        $result = $currentModel->delete();
        return $result;
    }
}
