<?php

namespace Stevema\Restful;

use Illuminate\Http\Resources\Json\JsonResource;

class RestfulResource extends JsonResource
{
    static $wrap = '';
}
