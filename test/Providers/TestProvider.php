<?php

namespace Stevema\Test\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;
use Stevema\Test\Models\SmUser;
use Stevema\Test\Observers\UserObserver;

class TestProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->registerRoutes();
        $this->registerMigrations();
        $this->registerPublishing();
        $this->registerCommands();
        $this->registerEvents();

        # 添加 observer  model::observer(ModelObserver::class)
//        \Stevema\Test\Models\SmTest::observe(\Stevema\Test\Observers\TestObserver::class);
        # 或者 protected $observers = [model::class => [ModelObserver:class]];
    }
    protected $observers = [
//        \Stevema\Test\Models\SmTest::class => [\Stevema\Test\Observers\TestObserver::class],
    ];

    protected function registerRoutes(){
        # 测试
        Route::prefix('t')
            ->name('t.')
            ->group(__DIR__.'/../Routes/t.php');
    }
    protected function registerMigrations(){
        if($this->app->runningInConsole()){
            $this->loadMigrationsFrom(__DIR__.'/../Database/migrations');
        }
    }
    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../database/migrations' => database_path('migrations'),
            ], 't.migrations');

//            $this->publishes([
//                __DIR__.'/../resources/views' => base_path('resources/views/vendor/passport'),
//            ], 't.views');

//            $this->publishes([
//                __DIR__.'/../config/passport.php' => config_path('passport.php'),
//            ], 't.config');
        }
    }
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Stevema\Test\Console\Command\TestCommand::class,
                \Stevema\Test\Console\Command\TestSeedCommand::class,
            ]);
        }
    }
    protected function registerEvents()
    {

        // 删除用户的时候把email 和 phone 唯一 给修改了
        SmUser::observe(UserObserver::class);

        if (!$this->app->runningInConsole()) {
            # 多个就写多个
//            Event::listen(
//                \Stevema\Test\Events\TestEvent::class,
//                \Stevema\Test\Listeners\TestListener::class
//            );
            # 多个就写多个
//            Event::subscribe(\Stevema\Test\Listeners\TestSubscriber::class);
        }
    }
}
