<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

# 测试


# Auth Controller绑定分组
Route::controller(\Stevema\Test\Controllers\AuthController::class)
    ->prefix('auth')
    ->name('auth.')->group(function () {
        Route::post('login', 'login')->name('login');
//        Route::post('register', 'register')->name('register');
        Route::get('logout', 'logout')->name('logout');
        Route::get('check', 'check')->name('check')->middleware('auth:api');
    });
# Afs
Route::get('afs', [\Stevema\Test\Controllers\AfsController::class, 'afs'])->name('afs');

# 普通分组
//    Route::prefix('afs')->name('afs.')->group(function(){
//        Route::get('create', \Stevema\Test\Controllers\AfsController.'@create')
//            ->name('create');
//        Route::get('check', \Stevema\Test\Controllers\AfsController.'@check')
//            ->name('check');
//    });

# invoke 单资源控制器
Route::get('invoke', \Stevema\Test\Controllers\InvokeController::class);
Route::get('sendmail', \Stevema\Test\Controllers\MailController::class);
Route::get('queue', \Stevema\Test\Controllers\QueueController::class);

# 资源控制器
# 部分  只要->only(”index“，”show“)   不要->except("destroy","update")
# 重命名 -names(['index' => 'aaa.index'])  重新定义变量 ->parameters(['resource' => 'photo'])
Route::resource('resource', \Stevema\Test\Controllers\ResourceController::class);

# API资源控制器
Route::apiResource('apiresource', \Stevema\Test\Controllers\ResourceController::class);

# event
Route::get('event', [\Stevema\Test\Controllers\EventController::class, 'event'])
    ->name('event');


Route::middleware('auth:api')->group(function(){
    # SmUser
    Route::apiResource('smuser', \Stevema\Test\Controllers\SmUserController::class);
    Route::get('smuser/{smuser}/restore',
        [\Stevema\Test\Controllers\SmUserController::class, 'restore']
    )->name('smuser.restore');
    # SmUser Profile - 一对一关系
    Route::get('smuser/{smuser}/profile',
        [\Stevema\Test\Controllers\SmUserController::class, 'profile']
    )->name('smuser.profile.show');
    Route::put('smuser/{smuser}/profile',
        [\Stevema\Test\Controllers\SmUserController::class, 'upprofile']
    )->name('smuser.profile.update');
    # SmUser - SmPost SmVideo  一对多关系
    Route::get('smuser/{smuser}/smposts',
        [\Stevema\Test\Controllers\SmUserController::class, 'smposts']
    )->name('smuser.smposts');
    Route::get('smuser/{smuser}/smvideos',
        [\Stevema\Test\Controllers\SmUserController::class, 'smvideos']
    )->name('smuser.smvideos');
    # SmUser - Smbadge  多对多关系
    Route::get('smuser/{smuser}/smbadges',
        [\Stevema\Test\Controllers\SmUserController::class, 'smbadges']
    )->name('smuser.smbadges');

    # SmBadge
    Route::apiResource('smbadge', \Stevema\Test\Controllers\SmBadgeController::class);

    # SmPost
    Route::apiResource('smpost', \Stevema\Test\Controllers\SmPostController::class);
    # SmComment
    Route::get('smpost/{smpost}/comments',
        [\Stevema\Test\Controllers\SmPostController::class, 'comments']
    )->name('smpost.comments');
    Route::post('smpost/{smpost}/comments',
        [\Stevema\Test\Controllers\SmPostController::class, 'addcomment']
    )->name('smpost.addcomment');

    # SmUser - Smbadge  多对多关系
    Route::get('smuser/{smuser}/smbadges',
        [\Stevema\Test\Controllers\SmUserController::class, 'smbadges']
    )->name('smuser.smbadges');

    # SmVideo
    Route::apiResource('smvideo', \Stevema\Test\Controllers\SmVideoController::class);

    # SmTag
    Route::apiResource('smtag', \Stevema\Test\Controllers\SmTagController::class);
    Route::get('smtag/{smtag}/restore',
        [\Stevema\Test\Controllers\SmTagController::class, 'restore']
    )->name('smuser.restore');
    # SmComment
    Route::apiResource('smcomment', \Stevema\Test\Controllers\SmCommentController::class)
        ->except('store','update');
});





