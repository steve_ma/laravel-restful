<?php

namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AfsController extends Controller
{
    public function afs(Request $request){
        $scene = $request->get('scene', 'nvc_login');
        $sessionId = $request->get('sessionId', 'stevema');
        $token = afs()->createToken($scene,$sessionId);
        return response()->json(['token' => $token]);
    }
}
