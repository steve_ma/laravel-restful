<?php

namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Token;

class AuthController extends Controller
{
    public function login(Request $request){

//        $user = User::create([
//            'name'      => "stevema",
//            'email'     => "471174757@qq.com",
//            'password'  => "shan0720"
//        ]);
        # 第一种 用户个人令牌授权  只能拿到token
        $user = User::find(1);
        $token = $user->createToken('api');
//        dump($token);
//        $accessToken = $token->accessToken;
//        dump($accessToken);
//        dd($token->token->id);
        $user->remember_token = $token->token->id;
        $user->save();
        $return = $user->toArray();
        $return['token'] = $token->accessToken;
        dd($return);
        return response()->json($user->toArray());
//        dd($user);
        # 第二种 密码模式授权
        $data = [
            'grant_type' => 'password',
            'client_id' => env('PASSPORT_PASSWORD_ACCESS_CLIENT_ID'),
            'client_secret' => env('PASSPORT_PASSWORD_ACCESS_CLIENT_SECRET'),
            'username' => "471174757@qq.com",
            'password' => "shan0720",
            'scope' => '*',
        ];
        $response = Http::asForm()->post(env('APP_URL').'/oauth/token', $data);
        return response()->json($response->json(), $response->status());
    }
    public function logout(){
        $user = auth('api')->user();
        if (empty($user)) {
            throw new AuthenticationException("还没有登录");
        }

        // 获取当前登陆用户的access_token的id
        $accessToken = request()->header('Authorization');
        $accessToken = str_replace("Bearer ", "", $accessToken);
        // 找到这条access_token并且将其删除
        $token = Token::find($accessToken);
        dd($user->getRememberToken());
        if (empty($token)) {
            return $this->sendError('暂无有效令牌', ['暂无有效令牌'], 403);
        }

        if (!empty($token->delete())) {
            return $this->sendResponse([], '退出成功！');
        } else {
            return $this->sendError('退出失败', ['退出失败'], 500);
        }
    }
    public function check(){
        return response()->json(auth()->user());
    }
}
