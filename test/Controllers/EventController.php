<?php

namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function event(){
        \Stevema\Test\Events\OrderSetLogEvent::dispatch("event1");
        event(new \Stevema\Test\Events\OrderSetLogEvent("event2"));
    }
}
