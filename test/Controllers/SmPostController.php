<?php

namespace Stevema\Test\Controllers;

use Illuminate\Support\Facades\Request;
use Stevema\Restful\RestfulController;
use Stevema\Restful\RestfulException;
use Stevema\Restful\Traits\Destroy;
use Stevema\Restful\Traits\Index;
use Stevema\Restful\Traits\Show;
use Stevema\Restful\Traits\Store;
use Stevema\Restful\Traits\Update;
use Illuminate\Support\Facades\DB;
use Stevema\Test\Models\SmPost;
use Stevema\Test\Models\SmTag;
use Stevema\Test\Requests\SmCommentRequest;
use Stevema\Test\Resources\SmCommentResource;

class SmPostController extends RestfulController
{
    use Index,Store,Update,Show,Destroy;
    /**
     * 路由绑定的参数名称
     * ROUTE_KEY 模型在路由中应当保持一致 然后就是应当是Model的小写
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * ROUTE_KEYMAP 关系模型时用到的
     * 比如 Route::get('/smorders/{smorder}/smskus/{smsku}', function(){})
     * SmOrder 和 SmSku 可以是一对一 也可以是一对多
     * 这样就需要 ROUTE_KEY来确认接收哪个参数了
     * ROUTE_KEYMAP 来对应相关的值 -> 在smsku 中 如果smorder关联的是 order_id
     * ROUTE_KEYMAP = ['smorder' => 'order_id]  这样来与smorder对应
     *
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * 只有一个模型的时候不需要 ROUTE_KEY 和 ROUTE_KEYMAP
     *
     * @var string
     */
    protected const ROUTE_KEY = null;
    protected const ROUTE_KEYMAP = [];
    /**
     * 过滤插件  列表传过来的参数需要一些方法来走到模型里面
     * 请继承 Stevema\Restful\Filters\RestfulFilter
     * 可以没有 但是不能乱搞
     */
    protected const FILTER = \Stevema\Test\Filters\SmPostFilter::class;
    /**
     * 模型 对应的表
     * 应当继承 Illuminate\Database\Eloquent\Model
     */
    protected const MODEL = \Stevema\Test\Models\SmPost::class;

    /**
     * 模型解释器 -> 模型查出来的数据 有些不想放出去的 就可以使用这个来处理
     * 应当继承 Illuminate\Http\Resources\Json\JsonResource
     */
    protected const RESOURCE = \Stevema\Test\Resources\SmPostResource::class;

    /**
     * request ->  post  put patch 使用到的表单验证 - 表单验证还包含权限验证
     * 应当继承 Illuminate\Foundation\Http\FormRequest
     * 继承其他的验证也可以
     */
    protected const REQUEST = \Stevema\Test\Requests\SmPostRequest::class;

    public function performStore($payload){
//        $model = static::getModel();
//        $currentModel = $model::create($payload)->refresh();
//        if(empty($currentModel)) throw new RestfulException("新增失败");
//        return $currentModel;
        return DB::transaction(function()use($payload){
            //事务
            $currentModel = \Stevema\Test\Models\SmPost::create($payload)->refresh();

            $arrs = [];
            if(!empty($payload['tags'])) {
                $tags = explode(",", $payload['tags']);
                foreach ($tags as $tag){
                    $tagModel = SmTag::withTrashed()->firstOrCreate(['name' => $tag]);
                    // deleted_at 是空 则没被删除
                    if(empty($tagModel->deleted_at)) {
                        $tagModel = SmTag::firstOrCreate(['name' => $tag]);
                        $arrs[] = $tagModel->id;
                    }
                }
            }


            // 关联一个
            // $post->tags()->attach($tag_id, []); // 数组内的可以加入到关联表的其它字段中
            // 关联多个
            // $post->tags()->attach([
            //    $tag_id => ['expires' => $expires],
            //    $tag_id => ['expires' => $expires],
            //]);
            // $post->tags()->attach([$tag_id，$tag_id，$tag_id]);
            //
            // 移除一个关联
            // $post->tags()->detach($tag_id);
            // 移除全部关联
            // $post->tags()->detach();
            // 同步关联
            // $post->tags()->sync([$tag_id, $tag_id, $tag_id]);
            // $post->tags()->sync([$tag_id =>[], $tag_id, $tag_id]);
            // 如果你想为每个同步的模型 IDs 插入相同的中间表 你可以使用 syncWithPivotValues 方法
            // $post->tags()->syncWithPivotValues([$tag_id, $tag_id, $tag_id], ['active' => true]);
            // 如果你不想移除现有的 IDs，可以使用 syncWithoutDetaching 方法：
            // $post->tags()->syncWithoutDetaching([1, 2, 3]);
            if(!empty($arrs)) {
                $currentModel->morphed_tags()->sync($arrs);
            }
            return $currentModel;
        });
//        DB::beginTransaction();
//        DB::rollBack();
//        DB::commit();

    }
    public function performUpdate($currentModel, $payload)
    {
        return DB::transaction(function()use($currentModel, $payload){
            //事务
            $currentModel->update($payload);

            $arrs = [];
            if(!empty($payload['tags'])) {
                $tags = explode(",", $payload['tags']);
                foreach ($tags as $tag){
                    $tagModel = SmTag::withTrashed()->firstOrCreate(['name' => $tag]);
                    // deleted_at 是空 则没被删除
                    if(empty($tagModel->deleted_at)) {
                        $tagModel = SmTag::firstOrCreate(['name' => $tag]);
                        $arrs[] = $tagModel->id;
                    }
                }
            }
            if(!empty($arrs)) {
                $currentModel->morphed_tags()->sync($arrs);
            }
//            throw new RestfulException("错误啦");
            return $currentModel;
        });
    }
    public function performDestroy($currentModel)
    {
        return DB::transaction(function()use($currentModel){
            //事务
            $currentModel->morphed_tags->detach();
            return \Stevema\Test\Models\SmPost::destroy($currentModel->id);
        });
    }

    public function comments(Request $request, $smpost){
        $currentModel = SmPost::withoutTrashed()
            ->with('comments')
            ->findOrFail($smpost);
        return response()->json([
            'list' => SmCommentResource::collection($currentModel->comments),
            'total' => intval($currentModel->comments_count)
        ]);

    }
    public function addcomment(SmCommentRequest $request, $smpost){
        $currentModel = SmPost::withoutTrashed()
            ->findOrFail($smpost);
        $payload = $request->only('content', 'user_id');
        $commentModel = DB::transaction(function()use($currentModel, $payload){
            //事务
            $commentModel = $currentModel->comments()->create($payload);
            return $commentModel;
        });
        return new SmCommentResource($commentModel);
    }
}
