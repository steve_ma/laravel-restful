<?php
namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QueueController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        # 队列
        \Stevema\Test\Jobs\OrderSetLogJob::dispatch('测试一个orderlog');
        \Stevema\Test\Jobs\SkuSetLogJob::dispatch('测试一个skulog');
//        info("开始执行事件");
//        sleep(5);
//        info("执行事件成功");
        return '立马返回数据';
    }
}
