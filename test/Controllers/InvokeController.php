<?php

namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvokeController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return "invoke - 只有一个方法";
    }
}
