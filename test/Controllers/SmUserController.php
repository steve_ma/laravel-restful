<?php

namespace Stevema\Test\Controllers;

use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Stevema\Restful\RestfulController;
use Stevema\Restful\RestfulException;
use Stevema\Restful\Traits\Destroy;
use Stevema\Restful\Traits\Index;
use Stevema\Restful\Traits\Show;
use Stevema\Restful\Traits\Store;
use Stevema\Restful\Traits\Update;
use Stevema\Restful\Traits\Restore;

use Stevema\Test\Models\SmBadge;
use Stevema\Test\Models\SmUser;
use Stevema\Test\Models\SmUserProfile;
use Stevema\Test\Requests\SmBadgeRequest;
use Stevema\Test\Requests\SmUserProfileRequest;
use Stevema\Test\Resources\SmBadgeResource;
use Stevema\Test\Resources\SmPostResource;
use Stevema\Test\Resources\SmUserProfileResource;
use Stevema\Test\Resources\SmVideoResource;

class SmUserController extends RestfulController
{
    use Index,Store,Update,Show,Destroy,Restore;
    /**
     * 路由绑定的参数名称
     * ROUTE_KEY 模型在路由中应当保持一致 然后就是应当是Model的小写
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * ROUTE_KEYMAP 关系模型时用到的
     * 比如 Route::get('/smorders/{smorder}/smskus/{smsku}', function(){})
     * SmOrder 和 SmSku 可以是一对一 也可以是一对多
     * 这样就需要 ROUTE_KEY来确认接收哪个参数了
     * ROUTE_KEYMAP 来对应相关的值 -> 在smsku 中 如果smorder关联的是 order_id
     * ROUTE_KEYMAP = ['smorder' => 'order_id]  这样来与smorder对应
     *
     * 比如 Route::get('/smorders/{smorder}', function(){}) -> 对应的模型应该是 SmOrder
     * 只有一个模型的时候不需要 ROUTE_KEY 和 ROUTE_KEYMAP
     *
     * @var string
     */
    protected const ROUTE_KEY = null;
    protected const ROUTE_KEYMAP = [];
    /**
     * 过滤插件  列表传过来的参数需要一些方法来走到模型里面
     * 请继承 Stevema\Restful\Filters\RestfulFilter
     * 可以没有 但是不能乱搞
     */
    protected const FILTER = \Stevema\Test\Filters\SmUserFilter::class;
    /**
     * 模型 对应的表
     * 应当继承 Illuminate\Database\Eloquent\Model
     */
    protected const MODEL = \Stevema\Test\Models\SmUser::class;

    /**
     * 模型解释器 -> 模型查出来的数据 有些不想放出去的 就可以使用这个来处理
     * 应当继承 Illuminate\Http\Resources\Json\JsonResource
     */
    protected const RESOURCE = \Stevema\Test\Resources\SmUserResource::class;

    /**
     * request ->  post  put patch 使用到的表单验证 - 表单验证还包含权限验证
     * 应当继承 Illuminate\Foundation\Http\FormRequest
     * 继承其他的验证也可以
     */
    protected const REQUEST = \Stevema\Test\Requests\SmUserRequest::class;

    public function profile($smuser){
        $profile = SmUserProfile::withoutTrashed()
            ->where('user_id', '=', $smuser)->first();
        if(empty($profile)){
            $profile = SmUserProfile::create([
                'user_id' => $smuser,
                'bio' => '',
                'city' => '',
                'hobby' => NULL
            ])->refresh();
        }
        return response()->json(new SmUserProfileResource($profile));
    }
    public function upprofile(SmUserProfileRequest $request, $smuser){
        $profile = SmUserProfile::withoutTrashed()
            ->where('user_id', '=', $smuser)->first();
        $res = $profile->update($request->only('bio','city','hobby'));
        if(!$res) throw new RestfulException("修改失败");
        return response()->json(new SmUserProfileResource($profile));
    }
    public function smbadges(Request $request, $smuser){
        $user = SmUser::withoutTrashed()->with('badges')->findOrFail($smuser);
        return response()->json(SmBadgeResource::collection($user->badges));
    }
    public function smposts(Request $request, $smuser){

        $post_query = function($query){
            $query->orderBy("id", 'desc')->offset(0)->limit(2);
        };
        $user = SmUser::withoutTrashed()
            ->with(['posts' => $post_query])
            ->withCount('posts')
            ->findOrFail($smuser);
        return response()->json([
            'list' => SmPostResource::collection($user->posts),
            'total' => $user->posts_count
        ]);
    }
    public function smvideos(Request $request, $smuser){
        $user = SmUser::withoutTrashed()
            ->with('videos')
            ->withCount('videos')
            ->findOrFail($smuser);
        return response()->json([
            'list' => SmVideoResource::collection($user->videos),
            'total' => $user->videos_count
        ]);
    }
}
