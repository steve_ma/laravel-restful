<?php

namespace Stevema\Test\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\TestMailSend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $res = Mail::to('471174757@qq.com')
            ->send(new TestMailSend("test one"));

        dd($res);
    }
}
