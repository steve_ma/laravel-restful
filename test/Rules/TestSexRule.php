<?php

namespace Stevema\Test\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class TestSexRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if(!in_array($value, [1,2])){
            $fail(":attribute的值只能是[1(男) 2(女)]其它的都是异端");
        }
    }
}
