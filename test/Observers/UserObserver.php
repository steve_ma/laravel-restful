<?php

namespace Stevema\Test\Observers;

use Stevema\Test\Models\SmUser as User;

class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        //
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
        $user->update([
            'email' => time() . '::' . $user->email,
            'phone' => time() . '::' . $user->phone
        ]);
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        $user->update([
            'email' => explode('::',$user->email)[1],
            'phone' => explode('::',$user->phone)[1]
        ]);
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
