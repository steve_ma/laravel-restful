<?php

namespace Stevema\Test\Console\Command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class TestSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restfultest:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commands description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->call('db:seed', [
            '--class' => 'Stevema\Test\Database\seeders\DatabaseSeeder'
        ]);
    }
}
