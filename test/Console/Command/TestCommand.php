<?php

namespace Stevema\Test\Console\Command;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restfultest:test {a=foo} {--b=*} {--queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commands description';

    protected function use(){
        # use Illuminate\Support\Facades\Artisan;
        # 程序化调用命令
        $exitCode = Artisan::call('restfultest:test', [
            'a' => 'aa',
            # 传递数组值
            '--b' => [12,13],
            # 传递布尔值
            '--queue' => true,
        ]);
        # 字符串
        $exitCode = Artisan::call('restfultest:test a=1 --b=12 --b=13 --queue');

        #队列 Artisan 命令
        Artisan::queue('stevema:test', [
            'a' => 'aa',
            # 传递数组值
            '--b' => [12,13],
            # 传递布尔值
            '--queue' => true,
        ]);
        # 使用 onConnection 和 onQueue 方法，你可以指定 Artisan 命令应分派到的连接或队列：
        # ->onConnection('redis')->onQueue('commands');

        # 从其他命令调用命令
        # handle 中编写
        $this->call('mail:send', [
            'user' => 1, '--queue' => 'default'
        ]);

        # 如果你想调用另一个控制台命令并禁止其所有输出，你可以使用 callSilently 方法。
        # callSilently 方法与 call 方法具有相同的签名：
        $this->callSilently('mail:send', [
            'user' => 1, '--queue' => 'default'
        ]);

        # 事件
        # Artisan 在运行命令时会调度三个事件：
        # Illuminate\Console\Events\ArtisanStarting，
        #Illuminate\Console\Events\CommandStarting 和
        # Illuminate\Console\Events\CommandFinished。
        #当 Artisan 开始运行时，会立即调度 ArtisanStarting 事件。
        #接下来，在命令运行之前立即调度 CommandStarting 事件。
        #最后，一旦命令执行完毕，就会调度 CommandFinished 事件。
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $a = $this->argument("a");
        $this->info($a);
        $b = $this->option("b");
        $this->line($b);
        $queue = $this->option('queue');
        if($queue) {
            $this->line("queue:true");
        } else {
            $this->error("queue:false");
        }
        # 输出空行
        $this->newLine();
        // 输出三个空行...
        $this->newLine(3);

        # 输入提示
        $name = $this->ask("give me your name:");
        $this->line("your name is {$name}");
        # 隐私的提示
        $secret = $this->secret("give me your secret name:");
        $this->line("your secret name is {$secret}");

        # 让用户确认
        if($this->confirm("Do you wish to continue?", true)){
            # y yes
            $this->line("yes");
        } else {
            # n no null
            $this->line("no");
        }
        # 自动完成 - 自动补全输入的值
        $name = $this->anticipate('What is your name?', ['Taylor', 'Dayle']);
        $this->line($name);

        # 给用户提供选择
        $name = $this->choice('What is your name?', ['Taylor', 'Dayle'], 0);
        $this->line($name);

        # 表格布局
        $this->table(
            ['Name', 'Email'],
//            User::all(['name', 'email'])->toArray()
            [
                ['Name' => 'name1', 'Email' => 'name1@qq.com'],
                ['Name' => 'name2', 'Email' => 'name2@qq.com'],
                ['Name' => 'name3', 'Email' => 'name3@qq.com'],
            ],
        );

        # 进度条
        $lists = range(1,5);
        $users = $this->withProgressBar($lists, function ($item) {
            $this->performTask($item);
        });

        # 输出空行
        $this->newLine();
        # 手动控制进度条
        $bar = $this->output->createProgressBar(count($lists));

        $bar->start();

        foreach ($lists as $item) {
            $this->performTask($item);
            $bar->advance();
        }

        $bar->finish();
        # 输出空行
        $this->newLine();
    }

    public function performTask($item){
//        $this->line($item);
        sleep(1);
    }
}
