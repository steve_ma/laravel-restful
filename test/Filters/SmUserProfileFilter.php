<?php
namespace Stevema\Test\Filters;

use Stevema\Restful\RestfulFilter;

class SmUserProfileFilter extends RestfulFilter{
    /**
     * 默认排序规则 多个中间加逗号  比如 "-type,-id"
     */
    protected const DEFAULT_ORDERING = '-id';
    /**
     * 允许的检索字段 keys  空则不限制 所有的参数都可以参与过滤
     * 注意这里只有id 没有前面的-号
     */
    protected const ACCEPT_ORDERINGS = ["id", 'created_at'];
    /**
     * 允许的检索字段 keys  空则不限制 所有的参数都可以参与过滤
     */
    protected const ACCEPT_FILTER_KEYS = [];

    /**
     * 分页用到的页码参数key
     */
    protected const KEY_PAGE = 'page';
    /**
     * 分页用到的每页显示条数参数key
     */
    protected const KEY_SIZE = 'size';
    /**
     * 分页用到的 cursor 参数key
     */
    protected const KEY_CURSOR = 'cursor';
    /**
     * 默认页数
     */
    protected const DEFAULT_PAGE = 1;
    /**
     * 默认条数
     */
    protected const DEFAULT_SIZE = 15;
    /**
     * 排序用到的参数key
     */
    protected const KEY_ORDERING = 'ordering';
    /**
     * 分页方法 nopaginate 无分页 、 paginate 默认分页 、 simplePaginate 简单分页 、 cursorPaginate cursor分页
     */
    protected const PAGINATOR = 'paginate';

    /**
     * 资源解释器 - 返回之前重新编辑一下输出的数组
     * 默认提供一个 可以用 也可以不用
     * \Stevema\Restful\Filters\RestfulFilterResource::class
     */
     protected const FILTER_RESOURCE = null;

    /**
     * 检索 自定义字段 比如 name字段  方法名是  name_filter
     * 可以获取 Builder 后执行操作
     * $queryset = $this->getQuerySet();
     * @param $key   字段名
     * @param $value  值
     * @return void
     */
    public function name_filter($key, $value){
        $query = $this->getQuery();
//        $query->where($key, 'like', "%{$value}%");
    }
}
