<?php

namespace Stevema\Test\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TestSubscriber
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handleTestEventAll($event) {
        echo 'eventAll:'.$event->a.PHP_EOL;
    }
    public function handleTestEvent1($event) {
        echo 'event1:'.$event->a.PHP_EOL;
    }
    public function handleTestEvent2($event) {
        echo 'event2:'.$event->a.PHP_EOL;
    }
    public function handle($event): void
    {
        //
    }
    public function subscribe($events): void
    {
        $events->listen(
            [
                \Stevema\Test\Events\OrderSetLogEvent::class,
                \Stevema\Test\Events\SkuSetLogEvent::class,
            ],
            [\Stevema\Test\Listeners\TestSubscriber::class, 'handleTestEventAll']
        );
        $events->listen(
            [
                \Stevema\Test\Events\OrderSetLogEvent::class,
            ],
            [\Stevema\Test\Listeners\TestSubscriber::class, 'handleTestEvent1']
        );
        $events->listen(
            [
                \Stevema\Test\Events\SkuSetLogEvent::class,
            ],
            [\Stevema\Test\Listeners\TestSubscriber::class, 'handleTestEvent2']
        );
    }
}
