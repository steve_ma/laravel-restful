<?php

namespace Stevema\Test\Listeners;

class OrderListener
{

    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     */
    public function handle(\Stevema\Test\Events\OrderSetLogEvent $event): void
    {
        echo $event->a.PHP_EOL;
    }
}
