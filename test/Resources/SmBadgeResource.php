<?php
namespace Stevema\Test\Resources;

use Illuminate\Http\Request;
use Stevema\Restful\RestfulResource;

class SmBadgeResource extends RestfulResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image
        ];
    }
}
