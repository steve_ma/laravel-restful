<?php
namespace Stevema\Test\Resources;

use Illuminate\Http\Request;
use Stevema\Restful\RestfulResource;

class SmCommentResource extends RestfulResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => new SmUserResource($this->user),
            'content' => $this->content,
            'created_at' => $this->created_at,
        ];
    }
}
