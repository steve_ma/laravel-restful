<?php
namespace Stevema\Test\Resources;

use Illuminate\Http\Request;
use Stevema\Restful\RestfulResource;

class SmUserProfileResource extends RestfulResource
{
    public function toArray(Request $request)
    {
        return [
            "bio" => $this->bio,
            "city" => $this->city,
            "hobby" => $this->hobby ? json_decode($this->hobby) : null,
        ];
    }
}
