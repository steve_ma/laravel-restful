<?php
namespace Stevema\Test\Resources;

use Illuminate\Http\Request;
use Stevema\Restful\RestfulResource;

class SmUserResource extends RestfulResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'phone' => $this->phone,
//            'profile' => new SmUserProfileResource($this->profile)
        ];
    }
}
