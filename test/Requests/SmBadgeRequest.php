<?php

namespace Stevema\Test\Requests;

use Stevema\Restful\RestfulRequest;
use Illuminate\Contracts\Validation\Validator;

class SmBadgeRequest extends RestfulRequest
{
    // 第一个失败后停止
    protected $stopOnFirstFailure = false;

    /**
     * 权限验证  false 会抛出权限异常中止请求
     * @return bool
     */
    public function authorize(): bool
    {
        return True;
//        $comment = Comment::find($this->route('comment'));
//        return $comment && $this->user()->can('update', $comment);
        //路由模型绑定 -  -
//        return $this->user()->can('update', $this->comment);
    }

    /**
     *  规则列表
     *  return [
     *      'name' => ['required','max:32'],
     *      'sex' => ['required'],
     *  ];
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [];
    }
    public $scenes = [
        'store' => [],
        'update' =>[]
    ];

    /**
     *  返回的提示
     * @return string[]
     */
    public function messages()
    {
        return [
//            'name.required' => ':attribute字段不能为空',
        ];
    }

    /**
     * 字段的明明包  可以把上面的:attribute 替换
     * @return string[]
     */
    public function attributes()
    {
        return [
//            'name' => '姓名',
        ];
    }

    /**
     * 准备验证数据。验证前数据处理
     * 比如把user_id 从header中取出来放进去
     */
    protected function prepareForValidation(): void
    {
//        var_dump("准备验证数据。验证前数据处理");
//        $this->merge([
//            'slug' => Str::slug($this->slug),
//            'user_id' => Auth()->user['id']
//        ]);
    }

    /**
     * 配置验证实例。- 验证后数据处理前 可以添加错误信息-
     * - - - 上面的规则总有一些是 不满足使用的
     * somethingElseIsInvalid 并不存在这个方法 就是告诉你这里可以有一些错误
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
//            if ($this->somethingElseIsInvalid()) {
//                $validator->errors()->add('field', 'Something is wrong with this field!');
//            }
        });
    }
    /**
     * 验证后数据处理
     * 有些数据是不想写入数据库的 比如 _token 等 就需要去掉
     * @return void
     */
    protected function passedValidation(): void
    {
//        var_dump("验证后数据处理");
//        $this->replace(['name' => 'Taylor']);
    }


}
