<?php

namespace Stevema\Test\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class AuthRegisterRequest extends FormRequest
{
    // 第一个失败后停止
    protected $stopOnFirstFailure = false;

    /**
     * 权限验证  false 会抛出权限异常中止请求
     * @return bool
     */
    public function authorize(): bool
    {
        return True;
    }

    /**
     *  规则列表
     *  return [
     *      'name' => ['required','max:32'],
     *      'sex' => ['required'],
     *  ];
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['max:20',],
            'phone' => ['required','regex:/^1[0-9]{1}\d{9}$/',],
            'password' => ['required',],
            'scene' => ['required',],
            'sessionid' => ['required',],
            'token' => ['required',],
        ];
    }

    /**
     *  返回的提示
     * @return string[]
     */
    public function messages()
    {
        return [
            'phone.required' => ':attribute不能为空',
            'phone.regex' => ':attribute不是正确的格式',
        ];
    }

    /**
     * 字段的明明包  可以把上面的:attribute 替换
     * @return string[]
     */
    public function attributes()
    {
        return [
            'phone' => '手机号',
        ];
    }

    /**
     * 验证失败后的错误处理
     * @param Validator $validator
     * @return mixed
     *
     */
    protected function failedValidation(Validator $validator){
        throw (new HttpResponseException(response($validator->errors()->messages(), 400)));
    }


    /**
     * 准备验证数据。验证前数据处理
     * 比如把user_id 从header中取出来放进去
     */
    protected function prepareForValidation(): void
    {
//        var_dump("准备验证数据。验证前数据处理");
//        $this->merge([
//            'slug' => Str::slug($this->slug),
//            'user_id' => Auth()->user['id']
//        ]);
    }

    /**
     * 配置验证实例。- 验证后数据处理前 可以添加错误信息-
     * - - - 上面的规则总有一些是 不满足使用的
     * somethingElseIsInvalid 并不存在这个方法 就是告诉你这里可以有一些错误
     * @param Validator $validator
     * @return void
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(function (Validator $validator) {
            $params = $validator->validated();
            if(!afs()->checkToken($params['scene'], $params['sessionid'], $params['token'])){
                $validator->errors()->add('token', afs()->getErrorMessage());
            }
//            if ($this->somethingElseIsInvalid()) {
//                $validator->errors()->add('field', 'Something is wrong with this field!');
//            }
        });
    }
    /**
     * 验证后数据处理
     * 有些数据是不想写入数据库的 比如 _token 等 就需要去掉
     * @return void
     */
    protected function passedValidation(): void
    {
        $password = $this->validated('password');
        $source = $this->validated('source', '默认值');
        $name = $this->validated('name', md5(time()));
        $avatar = $this->validated('avatar',"https://img01.wanfangche.com/public/upload/202012/24/5fe43566e6fdc.png?x-oss-process=style/small");
//        var_dump("验证后数据处理");
        $this->merge([
            'password' => md5($password),
            'source' => $source,
            'name' => $name,
            'avatar' => $avatar,
        ]);
//        $this->replace([
//            'password' => md5($password),
//            'source' => $source,
//            'name' => $name,
//            'avatar' => $avatar,
//        ]);
    }


}
