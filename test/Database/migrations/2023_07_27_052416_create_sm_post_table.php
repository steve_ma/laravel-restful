<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sm_post', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->comment("用户id");
            $table->string('title',255)->comment("标题");
            $table->string('keywords', 255)->comment("关键词");
            $table->text('describe')->comment("简介");
            $table->string('author', 50)->comment("作者");
            $table->text('poster')->comment("封面");
            $table->longText('content')->comment("内容");
            $table->text('tags')->nullable()->comment("内容匹配出来的关键词列表");
            $table->set('status', ['draft','published', 'closed'])->comment("状态");
            $table->dateTime('commit_at')->nullable()->comment("提交时间");
            $table->dateTime('published_at')->nullable()->comment("发布时间");
            $table->tinyInteger('is_comment_open')->default(1)->comment("评论开关");
            $table->tinyInteger('is_secret')->default(0)->comment("是否私密");
            $table->integer('read_num')->default(0)->comment("阅读数");
            $table->integer('comment_num')->default(0)->comment("评论数");
            $table->integer('like_num')->default(0)->comment("点赞数");
            $table->integer('favor_num')->default(0)->comment("收藏数");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sm_post');
    }
};
