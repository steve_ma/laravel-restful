<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sm_user_badge_relation', function (Blueprint $table) {
            $table->integer('user_id')->comment("用户id");
            $table->integer('badge_id')->comment("勋章id");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sm_user_badge_relation');
    }
};
