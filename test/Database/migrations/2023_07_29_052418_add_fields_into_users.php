<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone',50)->after('email_verified_at')->unique()->comment("手机号");
            $table->string('admin_role',20)->after("phone")->nullable()->comment("管理员的角色");
            $table->integer('is_admin')->after("admin_role")->default(0)->comment("是不是管理员");
            $table->integer('is_super_admin')->after("is_admin")->default(0)->comment("是不是超级管理员");
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
