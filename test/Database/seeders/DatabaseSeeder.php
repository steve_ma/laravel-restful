<?php

namespace Stevema\Test\Database\seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $files = File::files(__DIR__);
        foreach ($files as $file) {
            $classes = 'Stevema\\Test\\Database\\seeders\\' . $file->getFilenameWithoutExtension();
            if($classes !== __CLASS__){
                $this->call($classes);
            }
        }
    }
}
