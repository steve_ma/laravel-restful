<?php

namespace Stevema\Test\Database\seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Stevema\Test\Models\SmTest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SmTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //添加测试数据
        $st_id = DB::table("sm_test")->insert([
            'name' => Str::random(10),
            'password' => Hash::make('password'),
            'sex' => rand(1,2),
            'created_at' => date_create(),
            'updated_at' => date_create(),
        ]);
        DB::table("sm_test_sub")->insert([
            'st_id' => $st_id,
            'sub_name' => Str::random(10),
            'created_at' => date_create(),
            'updated_at' => date_create(),
        ]);
        DB::table("sm_test_subs")->insert([
            'st_id' => $st_id,
            'sub_name' => Str::random(10),
            'created_at' => date_create(),
            'updated_at' => date_create(),
        ]);
        DB::table("sm_test_subs")->insert([
            'st_id' => $st_id,
            'sub_name' => Str::random(10),
            'created_at' => date_create(),
            'updated_at' => date_create(),
        ]);
//        SmTest::created($data);
    }
}
