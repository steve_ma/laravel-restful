<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

class SmBadge extends RestfulModel
{
    use SoftDeletes;
    protected $table="sm_badge";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = ['name','image'];

    public function users()
    {
        return $this->belongsToMany(
            SmBadge::class,
            'post_tags',
            'badge_id',
            'user_id',
            'id',
            'id'
        );
    }
}
