<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

class SmComment extends RestfulModel
{
    use SoftDeletes;
    protected $table="sm_comment";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = ['user_id', 'content'];


    public function user(){
        return $this->belongsTo(SmUser::class, 'user_id', 'id');
    }

    public function commentable()
    {
        return $this->morphTo(
            'commentable',
            'commentable_type',
            'commentable_id',
            'id'
        );
    }
}
