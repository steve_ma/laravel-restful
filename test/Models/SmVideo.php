<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

class SmVideo extends RestfulModel
{
    use SoftDeletes;
    protected $table="sm_video";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = [
        'user_id','title','keywords','describe','author','poster',
        'content','tags','status','video',
    ];



    public function user()
    {
        return $this->belongsTo(SmUser::class, 'user_id', 'id');
    }


    public function morphed_tags()
    {
        return $this->morphedToMany(
            SmTag::class,
            'tag_able',
            'sm_tag_relation',
            'tag_able_id',
            'tag_id',
            'id',
            'id'
        );
    }

    public function comments()
    {
        return $this->morphMany(
            SmComment::class,
            'commentable',
            'commentable_type',
            'commentable_id',
            'id'
        );
    }
}
