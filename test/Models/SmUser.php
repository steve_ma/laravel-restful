<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class SmUser extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    # 一对一的 profile
    public function profile()
    {
        return $this->hasOne(SmUserProfile::class, 'user_id', 'id')->withDefault();;
    }
    # 一对多 post video
    public function posts()
    {
        return $this->hasMany(SmPost::class, 'user_id', 'id');
    }
    public function videos()
    {
        return $this->hasMany(SmVideo::class, 'user_id', 'id');
    }


    # 多对多的 badge
    public function badges()
    {
        return $this->belongsToMany(
            SmBadge::class,
            'sm_user_badge_relation',
            'user_id',
            'badge_id',
            'id',
            'id'
        );
    }


}
