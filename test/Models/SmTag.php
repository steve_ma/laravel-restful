<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

class SmTag extends RestfulModel
{
    use SoftDeletes;
    protected $table="sm_tag";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = ['name'];


    public function posts()
    {
        return $this->morphedByMany(
            SmPost::class,
            'tag_able',
            'sm_tag_relation',
            'tag_id',
            'tag_able_id',
            'id',
            'id'
        );
    }

    public function videos()
    {
        return $this->morphedByMany(
            SmVideo::class,
            'tag_able',
            'sm_tag_relation',
            'tag_id',
            'tag_able_id',
            'id',
            'id'
        );
    }

}
