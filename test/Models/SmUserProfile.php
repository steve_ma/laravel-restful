<?php
namespace Stevema\Test\Models;

use Stevema\Restful\RestfulModel;
use Stevema\Restful\Traits\SoftDeletes;

class SmUserProfile extends RestfulModel
{
    use SoftDeletes;
    protected $table="sm_user_profile";

    /**
     * 批量赋值的字段
     * @var string[]
     */
    protected $fillable = ['user_id','bio','city','hobby'];

    public function user(){
        return $this->belongsTo(SmUser::class, 'user_id', 'id');
    }
}
